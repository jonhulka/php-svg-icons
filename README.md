# php-svg-icons

Simple tool for extracting svg icons to a php configuration file and a class to build svg elements for output to html.

Licensed under GNU General Public License 3.0 or later (see LICENSE).
Feather icons included in example are licensed under the MIT license.
