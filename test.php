<?php
include __DIR__.'/IconWriter.class.php';
$writer=new IconWriter(__DIR__.'/feather-icons.php','feather');
echo '<style>
.feather{ width:100px; height:100px; fill:none; stroke-width:2; stroke:#888; stroke-linecap:round; stroke-linejoin:round; }
.feather:hover{ stroke-width:2.5; stroke:#444; }
</style>
'.$writer->getElement('at-sign');
