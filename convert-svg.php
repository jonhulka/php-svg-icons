<?php
/**
 * svg to php converter
 *
 *  Copyright 2019 by Jon Hulka (jon.hulka@gmail.com)
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */
header('Content-Type:text/plain');
echo <<<HEADER_TEXT
<?php
/**
 * see test.php for an example of writing an icon to output
*/

HEADER_TEXT;
//run this script in the directory where the .svg files are stored
$includeAttributes=false; //More efficient to use css for attributes
$files=scandir(__DIR__);
echo 'return [
';
foreach($files as $name) if(substr($name,-4)==='.svg')
{
	$path=__DIR__.'/'.$name;
	if(is_file($path))
	{
		$doc=new DOMDocument();
		$doc->load($path);
		$svg=$doc->firstChild;
		$inner='';
		$delim='';
		$attrs='\'a\'=>[';
		//viewbox attribute cannot be done in css, so include it here
		foreach($svg->attributes as $attr) if($attr->name=='viewBox' || $includeAttrs && $attr->name!='xmlns')
		{
			$attrs.=$delim.var_export($attr->name,true).'=>'.var_export($attr->value,true);
			$delim=',';
		}
		$attrs.=']';
		foreach($doc->firstChild->childNodes as $node) $inner.=$doc->saveXML($node);
		echo var_export(substr($name,0,-4)).'=>['.$attrs.',\'i\'=>'.var_export($inner,true).'],
';

	}
}
echo '];
';
