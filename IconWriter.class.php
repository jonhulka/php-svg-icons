<?php
/**
 * Utility class for writing svg images to html output
 *
 *  Copyright 2019 by Jon Hulka (jon.hulka@gmail.com)
 *
 *  Licensed under GNU General Public License 3.0 or later. 
 *
 * @license GPL-3.0+ <http://spdx.org/licenses/GPL-3.0+>
 */
class IconWriter
{
	protected
		$cssClass,
		$iconData;
	public function __construct($configPath,$cssClass=null)
	{
		$this->iconData= (include $configPath);
		$this->cssClass=$cssClass;
	}
	public function getElement($name)
	{
		$result='';
		if(!array_key_exists($name,$this->iconData)) throw new \Exception($name.' not found in icon list.');
		$icon=$this->iconData[$name];
		$result.='<svg xmlns="http://www.w3.org/2000/svg"';
		if(!empty($this->cssClass)) $result.=' class="'.htmlspecialchars($this->cssClass).'"';
		foreach($icon['a'] as $n=>$v) $result.=' '.$n.'="'.$v.'"';
		$result.='>'.$icon['i'].'</svg>';
		return $result;
	}
}
